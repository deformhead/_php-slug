<?php

function slug( $fullText ) {

    // encode each special character
    $parsedText = htmlentities( $fullText, ENT_COMPAT, 'utf-8', false ) ;

    // replace each encoded accented character by its related unaccented character
    $parsedText = preg_replace( '#&([[:alpha:]]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);#', '$1', $parsedText ) ;

    // decode each remaining special character
    $parsedText = html_entity_decode( $parsedText, ENT_COMPAT, 'utf-8' ) ;

    // replace each non alphanumeric character group by an hyphen
    $parsedText = preg_replace( '#([^[:alnum:]]+)#u', '-', $parsedText ) ;

    // replace each uppercase character by its related lowercase character
    $parsedText = strtolower( $parsedText ) ;

    // remove starting and ending hyphens
    $parsedText = trim( $parsedText, '-' ) ;

    return ( $parsedText ) ;
}
